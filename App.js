import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput, Alert } from 'react-native';
const App = () => {
 
  const [number, setNumber] = useState();
  const [textinput1, setTextinput1] = useState();
  const [textinput2, setTextinput2] = useState();
  const [textinput3, setTextinput3] = useState();
  const [condition, setCondition] = useState(false)

  const Dice = () => {
    setCondition(!condition)
    let Num = Math.floor(Math.random() * 6) + 1;
    console.log(`Dice:${Num}`);
    setNumber(Num)
    if(Num == textinput1 ){
      return Alert.alert('First Number Matched');
    }else if(Num == textinput2){
      return Alert.alert('Second Number Matched');
    }else if(Num == textinput3 ){
      return Alert.alert('Third Number Matched');
    }else{
      return Alert.alert('Number Not Matched');
    }  
  }
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center', margin: '10%' }}>
      <TouchableOpacity onPress={() => Dice()}>
        <View style={{ backgroundColor: '#ff6567', borderWidth: 2 }}>
          <Text style={{ color: 'white', fontSize: 30 }}>Press</Text>
        </View>
      </TouchableOpacity>
        <View style={{marginTop:'5%'}}> 
          {number== 1 ?
            <View>
              <Image source={require('./assets/1.png')} style={{ height: 100, width: 100 }}></Image>
            </View> : null}
          {number == 2 ?
            <View>
              <Image source={require('./assets/2.png')} style={{ height: 100, width: 100 }}></Image>
            </View> : null}
          {number == 3 ?
            <View>
              <Image source={require('./assets/3.png')} style={{ height: 100, width: 100 }}></Image>
            </View> : null}
          {number == 4 ?
            <View>
              <Image source={require('./assets/4.png')} style={{ height: 100, width: 100 }}></Image>
            </View> : null}
          {number ==5 ?
            <View>
              <Image source={require('./assets/5.png')} style={{ height: 100, width: 100 }}></Image>
            </View> : null}
          {number == 6 ?
            <View>
              <Image source={require('./assets/6.png')} style={{ height: 100, width: 100 }}></Image>
            </View> : null}
        </View>
        <View style={{marginTop:'5%',borderWidth:2,height:'15%',width:'60%'}}>
          <TextInput placeholder='Enter Any number from 1 to 6' onChangeText={(a)=>setTextinput1(a)}></TextInput>
        </View>  
        <View style={{marginTop:'5%',borderWidth:2,height:'15%',width:'60%'}}>
          <TextInput placeholder='Enter Any number from 1 to 6' onChangeText={(a)=>setTextinput2(a)}></TextInput>
        </View> 
        <View style={{marginTop:'5%',borderWidth:2,height:'15%',width:'60%'}}>
          <TextInput placeholder='Enter Any number from 1 to 6'  onChangeText={(a)=>setTextinput3(a)}></TextInput>
        </View>   
    </View>
  )
}
export default App;

// import React, { useState } from 'react';
// import { Text, TouchableOpacity, View, Image } from 'react-native';
// const App = () => {
//   const [num1, setNum1] = useState(false);
//   const [num2, setNum2] = useState(false);
//   const [num3, setNum3] = useState(false);
//   const [num4, setNum4] = useState(false);
//   const [num5, setNum5] = useState(false);
//   const [num6, setNum6] = useState(false);

//   const Dice = () => {
//     let Number = Math.floor(Math.random() * 6) + 1;
//     console.log(`Dice:${Number}`);
//     switch (Number) {
//       case 1:
//         return (
//           setNum1(true)
//         )
//       case 2:
//         return (
//           setNum2(true)
//         )
//       case 3:
//         return (
//           setNum3(true)
//         )
//       case 4:
//         return (
//           setNum4(true)
//         )
//       case 5:
//         return (
//           setNum5(true)
//         )
//       default:
//         return (
//           setNum6(true)
//         )
//     }
//   }
//   return (
//     <View>

//       <View style={{ alignSelf: 'center' }}>

//         {num1 == true ?
//           <View>
//             <Image source={require('./assets/1.png')}
//               style={{ height: 100, width: 100 }}></Image>
//           </View> : null}

//         {num2 == true ?
//           <View>
//             <Image source={require('./assets/2.png')}
//               style={{ height: 100, width: 100 }}></Image>
//           </View> : null}


//         {num3 == true ?
//           <View>
//             <Image source={require('./assets/3.png')}
//               style={{ height: 100, width: 100 }}></Image>
//           </View> : null}


//         {num4 == true ?
//           <View>
//             <Image source={require('./assets/4.png')}
//               style={{ height: 100, width: 100 }}></Image>
//           </View> : null}


//         {num5 == true ?
//           <View>
//             <Image source={require('./assets/5.png')}
//               style={{ height: 100, width: 100 }}></Image>
//           </View> : null}

//         {num6 == true ?
//           <View>
//             <Image source={require('./assets/6.png')}
//               style={{ height: 100, width: 100 }}></Image>
//           </View> : null}

//       </View>

//       <TouchableOpacity onPress={() => Dice()}>
//         <View style={{ alignSelf: 'center', borderWidth: 2, margin: '20%' }}>
//           <Text style={{ fontSize: 20, fontWeight: 'bold', }}>Roll Dice </Text>
//         </View>
//       </TouchableOpacity>
//     </View>

//   )
// }
// export default App;